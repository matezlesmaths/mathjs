#!/usr/bin/node

import { create, all } from 'mathjs'
const math = create(all)

const expression = math.parse(process.argv[2])

console.log('TXT : ' + expression.toString({ parenthesis: 'auto', implicit: 'hide' }))
console.log('TEX : ' + expression.toTex({ parenthesis: 'auto', implicit: 'hide' }))
